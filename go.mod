module gitlab.com/upyskills-packages/go/models-helpers

go 1.16

require (
	github.com/badoux/checkmail v1.2.1
	gorm.io/gorm v1.22.3
)

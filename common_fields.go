package models

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"gorm.io/gorm"
)

// const (
// 	UniqueViolationErr = pq.ErrorCode("23505")
// )
type ICommonModel interface {
	GetErrors() Errors
}

type LocationFields struct {
	CountryIsoCode string `gorm:"size:3;null" json:"countryIsoCode,omitempty"`
	ZipCode        string `gorm:"size:20;null" json:"zipCode,omitempty"`
	State          string `gorm:"size:100;null" json:"state,omitempty"`
	City           string `gorm:"size:100;null" json:"city,omitempty"`
}

type AuditFields struct {
	CreatedAt *time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"createdAt,omitempty"`
	UpdatedAt *time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updatedAt,omitempty"`
}

func (b *AuditFields) BeforeCreate(*gorm.DB) error {
	now := time.Now()
	b.CreatedAt = &now
	return nil
}

func (b *AuditFields) BeforeUpdate(*gorm.DB) error {
	now := time.Now()
	b.UpdatedAt = &now
	return nil
}

type CommonModel struct {
	ID uint64 `gorm:"primary_key;auto_increment" json:"id"`
	AuditFields
	Errors Errors `gorm:"-" json:"-"`
}

func (c *CommonModel) NewRecord() bool {
	return c.ID == 0
}

func (c CommonModel) GetErrors() Errors {
	return c.Errors
}

type CommonModelWithoutID struct {
	AuditFields
	Errors Errors `gorm:"-" json:"-"`
}

func (c CommonModelWithoutID) GetErrors() Errors {
	return c.Errors
}

type Errors struct {
	Exception      error
	Business       interface{}
	RecordNotFound error
}

func (e *Errors) Any() bool {
	return e.Exception != nil || e.RecordNotFound != nil || e.Business != nil
}

func permittedParams(params []byte, values string) ([]byte, error) {
	var unallowed []string
	inputParams := make(map[string]interface{})
	outputParams := make(map[string]interface{})

	json.Unmarshal(params, &inputParams)
	for k, v := range inputParams {
		if pos := strings.Index(values, k); pos == -1 {
			unallowed = append(unallowed, k)
		} else {
			outputParams[k] = v
		}
	}

	if len(unallowed) > 0 {
		log.Println(fmt.Errorf("params not allowed: %s", strings.Join(unallowed, ", ")))
	}

	return json.Marshal(outputParams)
}

func isDuplicateKeyError(err error) bool {
	// TODO: fazer funcionar
	// if err != nil && errors.Is(err, &pgconn.PgError{}) {
	// errs := error.(*pgconn.PgError)
	// if err != nil) {
	// 	return true
	// }
	return false
}

func dateDiff(a, b time.Time) (year, month, day, hour, min, sec int) {
	if a.Location() != b.Location() {
		b = b.In(a.Location())
	}
	if a.After(b) {
		a, b = b, a
	}
	y1, M1, d1 := a.Date()
	y2, M2, d2 := b.Date()

	h1, m1, s1 := a.Clock()
	h2, m2, s2 := b.Clock()

	year = int(y2 - y1)
	month = int(M2 - M1)
	day = int(d2 - d1)
	hour = int(h2 - h1)
	min = int(m2 - m1)
	sec = int(s2 - s1)

	// Normalize negative values
	if sec < 0 {
		sec += 60
		min--
	}
	if min < 0 {
		min += 60
		hour--
	}
	if hour < 0 {
		hour += 24
		day--
	}
	if day < 0 {
		// days in month:
		t := time.Date(y1, M1, 32, 0, 0, 0, 0, time.UTC)
		day += 32 - t.Day()
		month--
	}
	if month < 0 {
		month += 12
		year--
	}

	return
}

func periodDescription(startAt, endAt time.Time) string {
	years, months, _, _, _, _ := dateDiff(startAt, endAt)
	if years > 0 && months == 0 {
		return fmt.Sprintf("%v%s", years, yearLabel(years))
	}
	if months > 0 && years == 0 {
		return fmt.Sprintf("%v%s", months, monthLabel(months))
	}
	return fmt.Sprintf("%v%s %v%s", years, yearLabel(years), months, monthLabel(months))
}

func monthLabel(months int) string {
	if months > 1 {
		return "mos"
	}
	return "mo"
}

func yearLabel(years int) string {
	if years > 1 {
		return "yrs"
	}
	return "yr"
}

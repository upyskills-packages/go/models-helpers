package models

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/badoux/checkmail"
	"gorm.io/gorm"
)

type validateType struct {
	name  string
	param interface{}
}

type uniquenesScope struct {
	Name  string
	Value interface{}
}

func Validates(value interface{}, fieldError *[]string, types ...validateType) {
	for _, d := range types {
		switch d.name {
		case "presence":
			ValidatePresenceOf(value, fieldError)
		case "maxLength":
			ValidateMaxLengthOf(value, fieldError, d.param.(int))
		case "minLength":
			ValidateMinLengthOf(value, fieldError, d.param.(int))
		}
	}
}

func ValidatePresenceOf(value interface{}, fieldError *[]string) {
	switch value.(type) {
	case string:
		if len(strings.TrimSpace(value.(string))) > 0 {
			return
		}
	case uint64:
		if value.(uint64) > 0 {
			return
		}
	}
	appendError(fieldError, "Preenchimento Obrigatório")
}

func ValidateMaxLengthOf(value interface{}, fieldError *[]string, length int) {
	switch value.(type) {
	case string:
		if len(strings.TrimSpace(value.(string))) <= length {
			return
		}
	}
	appendError(fieldError, fmt.Sprintf("Tamaho máximo permitido é de %d", length))
}

func ValidateMinLengthOf(value interface{}, fieldError *[]string, length int) {
	switch value.(type) {
	case string:
		if len(strings.TrimSpace(value.(string))) >= length {
			return
		}
	}
	appendError(fieldError, fmt.Sprintf("Tamaho minimo permitido é de %d", length))
}

func ValidateEmail(value string, fieldError *[]string) {
	if err := checkmail.ValidateFormat(value); err != nil {
		appendError(fieldError, "Email Inválido")
	}
}

func ValidatesForStringFields(
	required bool,
	minLength interface{},
	maxLength interface{},
) []validateType {
	var types []validateType
	if required {
		types = append(types, validateType{name: "presence"})
	}
	if maxLength != nil && maxLength.(int) > 0 {
		types = append(types, validateType{name: "maxLength", param: maxLength})
	}
	if minLength != nil && minLength.(int) > 0 {
		types = append(types, validateType{name: "minLength", param: minLength})
	}
	return types
}

func ValidadeUniquenessOf(db *gorm.DB, fieldName string,
	value interface{}, fieldError *[]string, model interface{},
) {
	ValidadeUniquenessOfByScope(db, fieldName, value, fieldError, model, uniquenesScope{})
}

func ValidadeUniquenessOfByScope(db *gorm.DB, fieldName string,
	value interface{}, fieldError *[]string, model interface{}, scope uniquenesScope,
) {

	var errorDb *gorm.DB
	if scope.Name == "" {
		errorDb = db.Debug().Where(fmt.Sprintf("%v = ?", fieldName), value).First(model)
	} else {
		errorDb = db.Debug().Where(
			fmt.Sprintf("%v = ?", fieldName), value,
		).Where(
			fmt.Sprintf("%v = ?", scope.Name), scope.Value,
		).First(model)
	}
	if errorDb.RowsAffected > 0 {
		appendError(fieldError, "Deve ser ũnico")
	}
}

func ValidateInclusionOf(value interface{}, fieldError *[]string, options interface{}) {
	arr := reflect.ValueOf(options)
	for i := 0; i < arr.Len(); i++ {
		if arr.Index(i).Interface() == value {
			return
		}
	}
	appendError(fieldError, "Item not in list")
}

func appendError(fieldError *[]string, message string) {
	*fieldError = append(*fieldError, message)
}

func HasErrors(errors interface{}) bool {
	v := reflect.ValueOf(errors)
	for i := 0; i < v.NumField(); i++ {
		str := fmt.Sprintf("%v", v.Field(i).Interface())
		if str != "[]" {
			return true
		}
	}
	return false
}
